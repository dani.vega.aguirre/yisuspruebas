﻿namespace MODELOS
{
    public class Pelicula
    {
        public string ID { get; set; }
        public string NAME{ get; set; }

        public string GENRES { get; set; }

        public DateTime DATE { get; set; }

        public string STATE { get; set; }

    }
}