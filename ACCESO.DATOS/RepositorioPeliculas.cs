﻿using Dapper;
using MODELOS;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace ACCESO.DATOS
{
    public class RepositorioPeliculas
    {
       private readonly SQLConnection sQLConnection;
       public RepositorioPeliculas()
        {
            this.sQLConnection= new SQLConnection() { Database= "PELICULAS_DB", Host="localhost, 1433",Password= "Usr_Peliculas", User= "Usr_Peliculas" };
        }

        public List<Pelicula> GetList(FiltrosPeliculas filtros) 
        {

            using IDbConnection db = new SqlConnection(this.sQLConnection.ConnectionString);
            
            string storeProc = "[peli].[PELICULAGetList]";
            
            DynamicParameters parameters = new();
            string name = null;
            if(filtros.Name != "") name=filtros.Name;
            parameters.Add("@NAME", name, DbType.String, ParameterDirection.Input,50 );
            parameters.Add("@GENRES", null, DbType.String, ParameterDirection.Input, 50);
            parameters.Add("@DATE", null, DbType.Date, ParameterDirection.Input);
            parameters.Add("@STATE", null, DbType.String, ParameterDirection.Input, 50);
          
            List<Pelicula> peliculas = db.Query<Pelicula>(storeProc, parameters, commandType: CommandType.StoredProcedure).ToList();
            return peliculas;
        }

        public Pelicula Getbyid()
        {


            return new Pelicula();
        }

        public Pelicula PostList()
        {

            return new Pelicula();
        }

        public Pelicula putList()
        {

            return new Pelicula();
        }


    }
}