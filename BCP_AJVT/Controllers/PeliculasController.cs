﻿using Microsoft.AspNetCore.Mvc;
using MODELOS;
using SERVICIOS;

namespace BCP_AJVT.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeliculasController:ControllerBase
    {
        protected PeliculasServicio peliculasServicio;
        public PeliculasController()
        {
            peliculasServicio = new PeliculasServicio();
        }

        [HttpPost("GetList")]
        public IActionResult Get([FromBody] FiltrosPeliculas filtros)
        {
            List<Pelicula> x;
            try
            {
                 x = peliculasServicio.GetList(filtros);
            }
            catch(Exception e)
            {
                return this.BadRequest(e.Message);
            }
            return this.Ok(x);
        }
        [HttpGet("GetId")]
        public IActionResult Getbyid()
        {
            Pelicula y = peliculasServicio.Getbyid();
            return this.Ok("OK");
        }
        [HttpPost]
        public IActionResult Post()
        {
            Pelicula y = peliculasServicio.PostList();
            return this.Ok("OK");
        }
        [HttpPut]
        public IActionResult Put()
        {
            Pelicula y = peliculasServicio.putList();
            return this.Ok("OK");
        }

    }

}
